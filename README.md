# CORAppLinks

[![CI Status](https://img.shields.io/travis/Tomasz Pilarski/CORAppLinks.svg?style=flat)](https://travis-ci.org/Tomasz Pilarski/CORAppLinks)
[![Version](https://img.shields.io/cocoapods/v/CORAppLinks.svg?style=flat)](https://cocoapods.org/pods/CORAppLinks)
[![License](https://img.shields.io/cocoapods/l/CORAppLinks.svg?style=flat)](https://cocoapods.org/pods/CORAppLinks)
[![Platform](https://img.shields.io/cocoapods/p/CORAppLinks.svg?style=flat)](https://cocoapods.org/pods/CORAppLinks)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CORAppLinks is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CORAppLinks'
```

## Author

Tomasz Pilarski, core@boombit.com

## License

CORAppLinks is available under the MIT license. See the LICENSE file for more info.
