#import <UIKit/UIKit.h>

/// Callback type definition.
typedef void (*CORAppLinkCallback)(const char* appLinkUrl);

/// Class that is reponsilbe for receiving App Link when app opened and sending them to Unity part of module.
@interface CORAppLinks : NSObject


/// Object that contains last appLink used by application.
@property (nonatomic, retain, readonly) NSURL* appLink;


/// Callback from Unity parto of module.
@property (nonatomic) CORAppLinkCallback registeredCallback;

/// Shared instance od CORAppLinks.
+ (instancetype)sharedInstance;

/// Method invoked when continueUserActivity from UnityAppController is invoked.
/// @param userActivity Object with all infomration about user Activity.
- (void)continueUserActivity:(NSUserActivity *)userActivity;
 
@end
