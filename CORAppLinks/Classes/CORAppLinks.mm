#import "CORAppLinks.h"
#import <CORCommon/CORCommon.h>

@implementation CORAppLinks

@synthesize appLink;

+ (instancetype)sharedInstance {
    
    static id instance = nil;
    @synchronized (self) {
        if(instance == nil)
            instance = [[self alloc] init];
    }
    
    return instance;
}

- (void)continueUserActivity:(NSUserActivity *)userActivity
{
    if ([userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb])
    {
        appLink=userActivity.webpageURL;
        if (self.registeredCallback)
        {
            self.registeredCallback(appLink.absoluteString.UTF8String);
        }
    }
}

@end

extern "C" {

    bool _COROpenedFromAppLink()
    {
        if ([CORAppLinks sharedInstance].appLink) return YES;
        return NO;
    }

    const char* _CORGetAppLink()
    {
        if ([CORAppLinks sharedInstance].appLink)
        {
            return MakeStringCopy([CORAppLinks sharedInstance].appLink.absoluteString);
        }
        return nil;
    }

    void _CORRegisterAppLinksCallback(CORAppLinkCallback callback)
    {
        [CORAppLinks sharedInstance].registeredCallback=callback;
    }

}
